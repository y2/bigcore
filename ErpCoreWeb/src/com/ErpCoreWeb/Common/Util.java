package com.ErpCoreWeb.Common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.UUID;

import sun.misc.BASE64Encoder; 
import sun.misc.BASE64Decoder;

public class Util {
	

    //随机生成密码
    static public String RandPwd(int iCount)
    {
        String str = "1234567890qwertyuiopasdfghjklzxcvbnm";
        String sPwd = "";
        Random rand = new Random(System.currentTimeMillis());
        while (sPwd.length() < iCount)
        {
            int idx = rand.nextInt(str.length()-1);
            char letter = str.charAt(idx);
            sPwd = sPwd + letter;
        }
        return sPwd;
    }

    //判断字符串是否是数字
    //作者:甘孝俭
    public static Boolean IsNum(String str)
    {
        if (str==null || str.length()==0)
            return false;
        str = str.trim();
        if (str.length()==0)
            return false;
        for (int i = 0; i < str.length(); i++)
        {
            if (str.charAt(i) == '.') continue;
            if (str.charAt(i) == '+') continue;
            if (str.charAt(i) == '-') continue;
            if ((str.charAt(i) == 'e') || (str.charAt(i) == 'E')) continue; //科学计数法
            if ((str.charAt(i) < '0') || str.charAt(i) > '9')
                return false;
        }
        //不能多个小数点
        int idx = str.indexOf('.');
        if (idx != -1)
            if (str.indexOf('.', idx + 1) != -1)
                return false;
        return true;
    }
    //判断字符串是否是整数
    //作者:甘孝俭
    public static Boolean IsInt(String str)
    {
    	if (str==null || str.length()==0)
            return false;
        str = str.trim();
        if (str.length()==0)
            return false;
        for (int i = 0; i < str.length(); i++)
        {
            if ((str.charAt(i) < '0') || str.charAt(i) > '9')
                return false;
        }
        return true;
    }
    
    //转义json特殊字符
    public static void ConvertJsonSymbol(StringBuffer s)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            switch (c)
            {
                case '"':
                    sb.append("\\\"");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                case '/':
                    sb.append("\\/");
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                default:
                    sb.append(c);
                    break;
            }
        }
        s.setLength(0);
        s.append( sb.toString());

    }

    
	/**
    * 使用文件通道的方式复制文件
    * 
    * @param s
    *            源文件
    * @param t
    *            复制到的新文件
    */
    public static void fileChannelCopy(File s, File t) {
        FileInputStream fi = null;
        FileOutputStream fo = null;
        FileChannel in = null;
        FileChannel out = null;
        try {
            fi = new FileInputStream(s);
            fo = new FileOutputStream(t);
            in = fi.getChannel();//得到对应的文件通道
            out = fo.getChannel();//得到对应的文件通道
            in.transferTo(0, in.size(), out);//连接两个通道，并且从in通道读取，然后写入out通道
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fi.close();
                in.close();
                fo.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

 // 将 s 进行 BASE64 编码 
    public static String getBASE64(String s) { 
	    if (s == null) return null; 
	    return (new sun.misc.BASE64Encoder()).encode( s.getBytes() ); 
    } 

    // 将 BASE64 编码的字符串 s 进行解码 
    public static String getFromBASE64(String s) { 
	    if (s == null) return null; 
	    BASE64Decoder decoder = new BASE64Decoder(); 
	    try { 
	    byte[] b = decoder.decodeBuffer(s); 
	    return new String(b); 
	    } catch (Exception e) { 
	    return null; 
	    } 
    }
    
    //获取配置文件内容
    public static String getConfigValue(String sName)
    {
    	String sVal="";
		try {
			sVal = new  String(ResourceBundle.getBundle("config").getString(sName).getBytes("ISO8859-1"),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return sVal;
    }
    
}
