package com.ErpCoreWeb.View;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.CColumn;
import com.ErpCoreModel.Framework.CTable;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.UI.CColumnInView;
import com.ErpCoreModel.UI.CColumnInViewDetail;
import com.ErpCoreModel.UI.CView;
import com.ErpCoreModel.UI.CViewDetail;
import com.ErpCoreModel.UI.enumViewType;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class MasterDetailViewInfo2
 */
@WebServlet("/MasterDetailViewInfo2")
public class MasterDetailViewInfo2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

    public CView m_View = null;
    public UUID m_Catalog_id = Util.GetEmptyUUID();
    boolean m_bIsNew = false;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MasterDetailViewInfo2() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
            	response.getWriter().print("请重新登录！");
            	response.getWriter().close();
            	//response.getWriter().print("{success:false,err:'',url:'../Login.jsp'}");
				//response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
		
		String id = request.getParameter("id");
        if (!Global.IsNullParameter(id))
        {
            m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(id));
        }
        else
        {
            m_bIsNew = true;
            if (request.getSession().getAttribute("NewMasterDetailView") == null)
            {
//            	String url="MasterDetailViewInfo1.jsp?id=" + request.getParameter("id") + "&catalog_id=" + request.getParameter("catalog_id");
//                StringBuffer urlBf=new StringBuffer(url);
//                com.ErpCoreWeb.Common.Util.ConvertJsonSymbol(urlBf);
                try {
                	response.getWriter().print("时间超时！");
                	response.getWriter().close();
        			//response.getWriter().print(String.format("{success:false,err:'',url:'%s'}",urlBf.toString()));
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
                //Prev();
                return ;
            }
            else
            {
            	Map<UUID, CView> sortObj = (Map<UUID, CView>)request.getSession().getAttribute("NewMasterDetailView");
                m_View =(CView) sortObj.values().toArray()[0];
            }
        }
        String catalog_id = request.getParameter("catalog_id");
        if (!Global.IsNullParameter(catalog_id))
        {
            m_Catalog_id = Util.GetUUID(catalog_id);
        }
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("Next"))
        {
        	Next();
            return ;
        }
        else if (Action.equalsIgnoreCase("Cancel"))
        {
        	request.getSession().setAttribute("NewMasterDetailView", null);
            return ;
        }
	}

	void Next()
	{
		if (!ValidatePage2())
            return;
        if (!SavePage2())
            return;

    }
    boolean ValidatePage2()
    {
    	String MasterColumn=request.getParameter("MasterColumn");
    	String DetailColumn=request.getParameter("DetailColumn");
    	
        if (Global.IsNullParameter(MasterColumn))
        {
        	try {
				response.getWriter().print("请选择主表字段！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return false;
        }
        if (Global.IsNullParameter(DetailColumn))
        {
        	try {
				response.getWriter().print("请选择从表字段！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return false;
        }
        
        return true;
    }
    boolean SavePage2()
    {
    	String MasterColumn=request.getParameter("MasterColumn");
    	String DetailColumn=request.getParameter("DetailColumn");
    	String[] arrMCol = MasterColumn.split(",");
    	String[] arrDCol = DetailColumn.split(",");
        //主表
        CTable tableM = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_View.getFW_Table_id());
        List<CBaseObject> lstObj = tableM.getColumnMgr().GetList();
        int i=0;
        for (CBaseObject obj : lstObj)
        {
        	boolean bSelect=false;
        	for(String id:arrMCol)
        	{
        		if(id.equalsIgnoreCase(obj.getId().toString()))
        		{
        			bSelect=true;
        			break;
        		}
        	}
            if (bSelect)
            {
                CColumnInView civ = m_View.getColumnInViewMgr().FindByColumn(obj.getId());
                if (civ == null)
                {
                    civ = new CColumnInView();
                    civ.Ctx = Global.GetCtx(this.getServletContext());
                    civ.setUI_View_id ( m_View.getId());
                    civ.setFW_Table_id ( tableM.getId());
                    civ.setFW_Column_id ( obj.getId());
                    civ.setCaption ( ((CColumn)obj).getName());
                    civ.setIdx ( i);
                    m_View.getColumnInViewMgr().AddNew(civ);
                }
            }
            else
            {
                CColumnInView civ = m_View.getColumnInViewMgr().FindByColumn(obj.getId());
                if (civ != null)
                    m_View.getColumnInViewMgr().Delete(civ);
            }
            i++;
        }
        //从表
        CViewDetail ViewDetail = (CViewDetail)m_View.getViewDetailMgr().GetFirstObj();
        CTable tableD = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(ViewDetail.getFW_Table_id());
        lstObj = tableD.getColumnMgr().GetList();
        i=0;
        for (CBaseObject obj : lstObj)
        {
        	boolean bSelect=false;
        	for(String id:arrDCol)
        	{
        		if(id.equalsIgnoreCase(obj.getId().toString()))
        		{
        			bSelect=true;
        			break;
        		}
        	}
            if (bSelect)
            {
                CColumnInViewDetail civd = ViewDetail.getColumnInViewDetailMgr().FindByColumn(obj.getId());
                if (civd == null)
                {
                    civd = new CColumnInViewDetail();
                    civd.Ctx = Global.GetCtx(this.getServletContext());
                    civd.setUI_ViewDetail_id ( ViewDetail.getId());
                    civd.setFW_Table_id ( ViewDetail.getFW_Table_id());
                    civd.setFW_Column_id (obj.getId());
                    civd.setCaption ( ((CColumn)obj).getName());
                    civd.setIdx ( i);
                    ViewDetail.getColumnInViewDetailMgr().AddNew(civd);
                }
            }
            else
            {
                CColumnInViewDetail civd = ViewDetail.getColumnInViewDetailMgr().FindByColumn(obj.getId());
                if (civd != null)
                    ViewDetail.getColumnInViewDetailMgr().Delete(civd);
            }
            i++;
        }
        return true;
    }
    
}
